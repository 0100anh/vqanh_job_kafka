package com.viettel.bccs3.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.viettel.bccs3.domain.dto.Message;
import com.viettel.bccs3.domain.dto.Msg;

@Service
public final class ConsumerService {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    @KafkaListener(topics = "kafkaTopic", groupId = "group_id")
    public void consume(Message message) {
        logger.info(String.format("$$$$ => Consumed message: %s", message));
        if(message != null) {
        	List<Msg> lstMsg = message.getLstMsg();
        	for(Msg msg : lstMsg) {
        		logger.info(String.format("$$$$ => Consumed message: %s", msg.getMsg()));
        		//logger.info(msg.getMsg());
                System.out.println("ngon roi");
        	}
        }
    }
}
