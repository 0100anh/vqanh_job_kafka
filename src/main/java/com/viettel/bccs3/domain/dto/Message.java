package com.viettel.bccs3.domain.dto;

import java.util.List;

public class Message {
	private List<Msg> lstMsg;
	private List<Name> lstName;
	public List<Msg> getLstMsg() {
		return lstMsg;
	}
	public void setLstMsg(List<Msg> lstMsg) {
		this.lstMsg = lstMsg;
	}
	public List<Name> getLstName() {
		return lstName;
	}
	public void setLstName(List<Name> lstName) {
		this.lstName = lstName;
	}
	
//	private String msg;
//	private String name;
//	public String getMsg() {
//		return msg;
//	}
//	public void setMsg(String msg) {
//		this.msg = msg;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
	
}
