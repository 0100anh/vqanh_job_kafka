package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@Entity
@NoArgsConstructor
@Builder
@Table(name = "INVOICE_INFO_DISCOUNT")
public class InvoiceInfoDiscount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INVOICE_INFO_DISCOUNT_ID")
    private Long invoiceInfoDiscountId;
    @Column(name = "INVOICE_INFO_ID")
    private Long invoiceInfoId;
    @Column(name = "LINE_NUMBER")
    private String lineNumber;
    @Column(name = "UNIT_NAME")
    private String unitName;
    @Column(name = "ITEM_NAME")
    private String itemName;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "TAX_AMOUNT")
    private Long taxAmount;
    @Column(name = "ITEM_ID")
    private Long itemId;
    @Column(name = "ITEM_TOTAL_AMOUNT_NOT_TAX")
    private Long itemTotalAmountNotTax;
    @Column(name = "TAX_PERCENTAGE")
    private String taxPerCenTage;
    @Column(name = "UNIT_PRICE")
    private Long unitPrice;

}
