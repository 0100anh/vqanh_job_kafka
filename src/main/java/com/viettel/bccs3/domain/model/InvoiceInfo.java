package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE_INFO")
public class InvoiceInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "INVOICE_INFO_ID")
    private Long invoiceInfoId;
    @Column (name = "CREATE_DATE")
    private Date createDate;
    @Column (name = "TEMPLATE_CODE")
    private String templateCode;
    @Column (name = "INVOICE_NOTE")
    private Long invoiceNote;
    @Column (name = "ADDITIONAL_REFERENCE_DATE")
    private Date additionalReferenceDate;
    @Column (name = "BUYER_ADDRESS")
    private String buyerAddress;
    @Column (name = "BUYER_EMAIL")
    private String buyerEmail;
    @Column (name = "SELLER_TAX_CODE")
    private String sellerTaxCode;
    @Column (name = "TOTAL_AMOUNT_WITH_TAX_IN_WORD")
    private String totalAmountWithTaxInWord;
    @Column (name = "INVOICE_SYSTEM")
    private Long invoiceSystem;
    @Column (name = "INVOICE_NUMBER")
    private String invoiceNumber;
    @Column (name = "PAYMENT_TYPE_NAME")
    private String paymentTypeName;
    @Column (name = "SELLER_ADDRESS_LINE")
    private String sellerAddressLine;
    @Column (name = "DISCOUNT_AMOUNT")
    private Long discountAmount;
    @Column (name = "TAX_CODE")
    private String taxCode;
    @Column (name = "INVOICE_SERIES")
    private String invoiceSeries;
    @Column (name = "BUYER_BANK_NAME")
    private String buyerBankName;
    @Column (name = "BUYER_PHONE_NUMBER")
    private String buyerPhoneNumber;
    @Column (name = "EXT_ATTRIBUTE_KEY")
    private String extAttributeKey;
    @Column (name = "EXT_ATTRIBUTE_VALUE")
    private String extAttributeValue;
    @Column (name = "SELLER_LEGAL_NAME")
    private String sellerLegalName;
    @Column (name = "SALLER_BANK_ACCOUNT")
    private String sallerBankAccount;
    @Column (name = "PROCESS_TYPE")
    private Long processType;
    @Column (name = "ADJUSTMENT_TYPE")
    private Long adjustmentType;
    @Column (name = "INVOICE_TYPE")
    private String invoiceType;
    @Column (name = "CUST_GET_INVOICE_RIGHT")
    private Long custGetInvoiceRight;
    @Column (name = "BUYER_NAME")
    private String buyerName;
    @Column (name = "PAYMENT_METHOD_NAME")
    private String paymentMethodName;
    @Column (name = "TOTAL_TAX_AMOUNT")
    private Long totalTaxAmount;
    @Column (name = "CONTRACT_NUMBER")
    private String contractNumber;
    @Column (name = "BUYER_BANK_ACCOUNT")
    private String buyerBankAccount;
    @Column (name = "SELLER_BANK_NAME")
    private String sellerBankName;
    @Column (name = "INVOICE_ISSUE_DATE")
    private Date invoiceIssueDate;
    @Column (name = "INVOICE_NO")
    private String invoiceNo;
    @Column (name = "PAYMENT_STATUS")
    private String paymentStatus;
    @Column (name = "BUYER_TAX_CODE")
    private String buyerTaxCode;
    @Column (name = "INVOICE_USED_ID")
    private Long invoiceUsedId;
    @Column (name = "CURRENCY_CODE")
    private String currencyCode;
    @Column (name = "INVOICE_NAME")
    private String invoiceName;
    @Column (name = "ORIGINAL_INVOICE_ID")
    private Long originalInvoiceId;
    @Column (name = "ORIGINAL_INVOICE_ISSUE_DATE")
    private Date originalInvoiceIssueDate;
    @Column (name = "TOTAL_AMOUNT_WITHOUT_TAX")
    private Long totalAmountWithoutTax;
    @Column (name = "TOTAL_AMOUNT_WITH_TAX")
    private Long totalAmountWithTax;
    @Column (name = "PAYMENT_TYPE")
    private String paymentType;
    @Column (name = "SUM_TOTAL_LINE_AMOUNT_NOT_TAX")
    private Long sumTotalLineAmountNotTax;

}
