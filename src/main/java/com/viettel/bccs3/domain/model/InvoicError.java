package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "INVOICE_ERROR")
public class InvoicError {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "PK_ID")
    private Long pkId;
    @Column(name = "ISSUE_DATE")
    private Date issueDate;
    @Column(name = "BRANCH_ID")
    private Long branchId;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;
    @Column(name = "PK_TYPE")
    private Long pkType;
}
