package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "INVOICE_PROCESS")
public class InvoiceProcess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "BRANCH_ID")
    private Long branchId;
    @Column(name = "INVOICE_DATE")
    private Date invoiceDate;
    @Column(name = "CREATE_DATE")
    private Date createDate;
    @Column(name = "LAST_MODIFY")
    private Date lastModify;
    @Column(name = "STATUS")
    private Long status;
}
