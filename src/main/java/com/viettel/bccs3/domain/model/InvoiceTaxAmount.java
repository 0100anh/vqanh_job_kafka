package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE_TAX_AMOUNT")
public class InvoiceTaxAmount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INVOICE_INFO_TAX_ID")
    private Long invoiceInfoTaxId;
    @Column(name = "INVOICE_INFO_ID")
    private Long invoiceInfoId;
    @Column(name = "TAX_PERCENTAGE")
    private String taxPercenTage;
    @Column(name = "TAX_AMOUNT")
    private Long taxAmount;
    @Column(name = "TAX_ABLE_AMOUNT")
    private Long taxAbleAmount;
}
