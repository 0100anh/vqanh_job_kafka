package com.viettel.bccs3.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE_INFO_ITEMS")
public class InvoiceInfoItems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INVOICE_INFO_ITEMS_ID")
    private Long invoiceInfoItemsId;
    @Column(name = "INVOICE_INFO_ID")
    private Long invoiceInfoId;
    @Column(name = "UNIT_PRICE")
    private Long unitPrice;
    @Column(name = "ITEM_TOTAL_AMOUNT_NOT_TAX")
    private Long itemTotalAmountNotTax;
    @Column(name = "LINE_NUMBER")
    private String lineNumber;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "UNIT_NAME")
    private String unitName;
    @Column(name = "TAX_AMOUNT")
    private Long taxAmount;
    @Column(name = "ITEM_ID")
    private Long itemId;
    @Column(name = "ITEM_NAME")
    private String itemName;
    @Column(name = "TAX_PERCENTAGE")
    private String taxPercenTage;
}
