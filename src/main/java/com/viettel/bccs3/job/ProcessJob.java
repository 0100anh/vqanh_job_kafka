package com.viettel.bccs3.job;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProcessJob {

	private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss sss");

	@Scheduled(cron = "*/5 * * * * ?")
//	@Scheduled(fixedDelay = 2 * 1000, initialDelay = 30 * 1000)
	public void process() {
		Date now = new Date();
		String nowString = df.format(now);
		System.out.println("Now is: " + nowString + " 7777777 ");
	}
}
