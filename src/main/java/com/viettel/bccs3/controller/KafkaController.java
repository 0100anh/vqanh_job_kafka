package com.viettel.bccs3.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.bccs3.domain.dto.Message;
import com.viettel.bccs3.domain.dto.Msg;
import com.viettel.bccs3.domain.dto.Name;
import com.viettel.bccs3.service.ProducerService;

@RestController
@RequestMapping("/kafka")
public final class KafkaController {
    private final ProducerService producerService;

    public KafkaController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @PostMapping(value = "/publish")
    public void sendMessageToKafkaTopic(@RequestParam String message) {
    	Message obj = new Message();
    	List<Msg> lstMsg = new ArrayList<>();
    	List<Name> lstName = new ArrayList<>();
    	Msg msg = new Msg();
    	msg.setMsg(message);
    	Name name = new Name();
    	name.setName("name");
    	lstMsg.add(msg);
    	lstName.add(name);
    	obj.setLstMsg(lstMsg);
    	obj.setLstName(lstName);
        producerService.sendMessage(obj);
    }
}
